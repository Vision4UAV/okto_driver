#! /usr/bin/env python

from MkMsg import *

class VersionRequestMsg(MkMsg):
    def __init__(self):
        self.address = MkMsg.ALL_ADDRESS
        self.cmd = 'v'
        self.data = []


class VersionMsg(MkMsg):
    def __init__(self, msg):
        self.address = None
        self.cmd = None
        self.data = None
        
        self.parse(msg)
        if (self.cmd != 'V'):
            raise InvalidMsgType
            
    def getVersion(self):
        return (self.data[0], self.data[1])
