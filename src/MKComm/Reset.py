#! /usr/bin/env python

from MkMsg import *

class ResetMsg(MkMsg):
    def __init__(self):
        self.address = MkMsg.ALL_ADDRESS
        self.cmd = 'R'
        self.data = []
