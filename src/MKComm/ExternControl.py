#! /usr/bin/env python

from MkMsg import *
import copy

class ExternControlRequestMsg(MkMsg):
    def __init__(self):
        self.address = MkMsg.ALL_ADDRESS
        self.cmd = 'g'
        self.data = []

        
class ExternControlMsg(MkMsg):
    def __init__(self, exCtrlRequestData):
        self.address = MkMsg.ALL_ADDRESS
        self.cmd = 'b'
        # [Digital[0], Digital[1], RemoteButtons, Nick, Roll, Yaw, Gas, Height, Free, Frame, Config]
        self.data = copy.copy(exCtrlRequestData)
        self.data[-1] = 1
        self.data[-2] = 1
        
    def changeNick(self, nick):
        self.data[3] = nick
        
    def changeRoll(self, roll):
        self.data[4] = roll
        
    def changeYaw(self, yaw):
        self.data[5] = yaw
        
    def changeGas(self, gas): 
        self.data[6] = gas
        
    def changeHeight(self, height):
        self.data[7] = height
        
    def changeAll(self, nick=None, roll=None, yaw=None, gas=None, height=None):
        if nick != None:
            self.data[3] = nick
            
        if roll != None:
            self.data[4] = roll
            
        if yaw != None:
            self.data[5] = yaw
            
        if gas != None:
            self.data[6] = gas
            
        if height != None:
            self.data[7] = height
            
