#! /usr/bin/env python

from MkMsg import *

# may not be used, as firmware may be modified to send this automatically without needing an explicit request
class DebugDataRequestMsg(MkMsg):
    def __init__(self):
        self.address = MkMsg.ALL_ADDRESS
        self.cmd = 'd'
        self.data = [500]


class DebugDataMsg(MkMsg):
    IDX_ANGNICK      =   2+2*0
    IDX_ANGROLL      =   2+2*1
    IDX_ACCNICK      =   2+2*2
    IDX_ACCROLL      =   2+2*3
    IDX_YAWGYRO      =   2+2*4
    IDX_ALTITUDE     =   2+2*5
    IDX_ACCZ         =   2+2*6
    IDX_COMPASS      =   2+2*8
    IDX_VOLTAGE      =   2+2*9
    IDX_GYROCOMPASS  =   2+2*11
    IDX_CURRENT      =   2+2*22
    IDX_CAPACITY     =   2+2*23
    IDX_GPSNICK      =   2+2*30
    IDX_GPSROLL      =   2+2*31

    def __init__(self, msg):
        self.address = None
        self.cmd = None
        self.data = None
        
        self.parse(msg)
        if (self.cmd != 'D'):
            raise InvalidMsgType

    def getAngNick(self):
        return self.data2SignedInt(DebugDataMsg.IDX_ANGNICK);
        
    def getAngRoll(self):
        return self.data2SignedInt(DebugDataMsg.IDX_ANGROLL);
    
    def getAccNick(self):
        return self.data2SignedInt(DebugDataMsg.IDX_ACCNICK);

    def getAccRoll(self):
        return self.data2SignedInt(DebugDataMsg.IDX_ACCROLL);
        
    def getYawGyro(self):
        return self.data2SignedInt(DebugDataMsg.IDX_YAWGYRO);
        
    def getAltitude(self):
        return float(self.data2SignedInt(DebugDataMsg.IDX_ALTITUDE))/10;
        
    def getAccZ(self):
        return self.data2SignedInt(DebugDataMsg.IDX_ACCZ);

    def getCompassHeading(self):
        return self.data2SignedInt(DebugDataMsg.IDX_COMPASS);

    def getVoltage(self):
        return float(self.data2SignedInt(DebugDataMsg.IDX_VOLTAGE))/10;
        
    def getGyroCompassHeading(self):
        return self.data2SignedInt(DebugDataMsg.IDX_GYROCOMPASS);
        
    def getCurrent(self):
        return float(self.data2SignedInt(DebugDataMsg.IDX_CURRENT))/10;
        
    def getCapacity(self):
        return self.data2SignedInt(DebugDataMsg.IDX_CAPACITY);
        
    def getGPSNick(self):
        return self.data2SignedInt(DebugDataMsg.IDX_GPSNICK);
        
    def getGPSRoll(self):
        return self.data2SignedInt(DebugDataMsg.IDX_GPSROLL);
