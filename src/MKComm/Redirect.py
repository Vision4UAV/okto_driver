#! /usr/bin/env python

from MkMsg import *

class NCRedirectToFCMsg(MkMsg):
    def __init__(self):
        self.address = MkMsg.NC_ADDRESS
        self.cmd = 'u'
        self.data = [0]
        
        
# special message to redirect uart back to NC (does not follow normal mk serial protocol)
class RedirectToNCMsg:
    def __init__(self):
        pass
        
    def generateMsg(self):
        return '\x1b\x1bU\xaa\x00'
