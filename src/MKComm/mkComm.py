#!/usr/bin/env python

from MkMsg import *
from DebugOut import *
from ExternControl import *
from EngineTest import *
from Redirect import *
from Reset import *
from Version import *
import serial
import time
import traceback

class MkComm:
    ALL_ADDRESS     = 0
    FC_ADDRESS      = 1
    NC_ADDRESS      = 2
    MK3MAG_ADDRESS  = 3
    BL_CTRL_ADDRESS = 5
    
    def __init__(self, printDebugMsg=False):
        self.serPort = None
        self.printDebugMsg = printDebugMsg
        self.uartState = MkComm.NC_ADDRESS
        self.externControl = None
        
    def open(self, comPort):
        self.serPort = serial.Serial(comPort, 57600, timeout=0.5)
        if not self.serPort.isOpen():
            raise IOError("Failed to open serial port")

    def close(self):
        self.serPort.close()
        
    def isOpen(self):
        return self.serPort != None

    def sendLn(self, ln):
        self.serPort.write(ln)

    def waitForLn(self):
        msg = self.serPort.read()
        while msg == '' or msg[-1] != '\r':
            msg += self.serPort.read()
        return msg

    def waitForMsg(self, cmd2wait4):
        msg = None
        done = False
        while (not done):
            line = self.waitForLn()
            #print line
            if len(line) == 0:
                raise NoResponse(cmd2wait4)
            try:
                #msg = MkMsg(msg=line)
                #if (msg.cmd == cmd2wait4):
                if (line[2] == cmd2wait4):
                    done = True
            except InvalidMsg:
                if self.printDebugMsg:
                  print "DebugMsg: \"%s\"" % line[:-1]
                pass
        #return msg
        return line

    def redirectNCToFC(self):
        msg = NCRedirectToFCMsg()
        self.sendLn(msg.generateMsg())
        self.uartState = MkComm.FC_ADDRESS
        time.sleep(.5) 
    
    def redirectToNC(self):
        msg = RedirectToNCMsg()
        self.sendLn(msg.generateMsg())
        self.uartState = MkComm.NC_ADDRESS
        time.sleep(.5)
          
    def getExternControl(self):
        self.serPort.flushInput();
        msg = ExternControlRequestMsg()
        self.sendLn(msg.generateMsg())
        line = self.waitForMsg('G')
        response = MkMsg(msg=line)
        self.externControl = ExternControlMsg(response.data) 
        return response
            
    def changeNick(self, nick):
        if self.externControl != None:
            self.externControl.changeNick(nick)
            self.sendLn(self.externControl.generateMsg())
            return True
        else:
            return False
            
    def changeRoll(self, roll):
        if self.externControl != None:
            self.externControl.changeRoll(roll)
            self.sendLn(self.externControl.generateMsg())
            return True
        else:
            return False
        
    def changeYaw(self, yaw):
        if self.externControl != None:
            self.externControl.changeYaw(yaw)
            self.sendLn(self.externControl.generateMsg())
            return True
        else:
            return False
        
    def changeGas(self, gas):
        if self.externControl != None:
            self.externControl.changeGas(gas)
            self.sendLn(self.externControl.generateMsg())
            return True
        else:
            return False
        
    def changeHeight(self, height):
        if self.externControl != None:
            self.externControl.changeHeight(height)
            self.sendLn(self.externControl.generateMsg())
            return True
        else:
            return False
    
    def changeAll(self, nick=None, roll=None, yaw=None, gas=None, height=None):
        if self.externControl != None:
            self.externControl.changeAll(nick=nick, roll=roll, yaw=yaw, gas=gas, height=height)
            self.sendLn(self.externControl.generateMsg())
        else:
            return False
        
    def reset(self):
    	msg = ResetMsg()
    	self.sendLn(msg.generateMsg())
    	self.uartState = MkComm.NC_ADDRESS
    	time.sleep(10)    

    def getDebugData(self):
        self.serPort.flushInput()
        #msg = DebugDataRequestMsg()
        #self.sendLn(msg.generateMsg())
        line = self.waitForMsg('D')
        response = DebugDataMsg(line)
        return response

    def getVersion(self):
        self.serPort.flushInput()
        msg = VersionRequestMsg()
        self.sendLn(msg.generateMsg())
        line = self.waitForMsg('V')
        response = VersionMsg(line)
        return response
        
    def setMotors(self, motorspeeds):
        if self.uartState == MkComm.FC_ADDRESS:
            msg = EngineTestMsg(motorspeeds)
            self.sendLn(msg.generateMsg())
            return True
        else:
            return False
        
    def getUARTState(self):
    	return self.uartState
    	


if __name__ == '__main__':
    try:
        comm = MkComm()
        comm.open(comPort="/dev/ttyUSB0")
        
        #comm.reset()
        
        #msg = comm.getVersion()
        #print "Version: %d.%d" % msg.getVersion()
        
        print "switching to FC"
        comm.redirectNCToFC()
        print "switched to FC"
        
        #msg = comm.getVersion()
        #print "Version: %d.%d" % msg.getVersion()
        
        #print "switching to NC"
        #comm.redirectToNC()
        #print "switched to NC"
        
        #msg = comm.getVersion()
        #print "Version: %d.%d" % msg.getVersion()
        
        #print "Running Engine Test"
        #comm.setMotors([10,10,10,10,10,10,10,10,0,0,0,0])
        
        #print "switching to NC"
        #comm.redirectToNC()
        #print "switched to NC"
        
        #while True:
        #    msg = comm.getDebugData()
        #    print "Compass: %d" % msg.getCompassHeading()
        
        print "Sending ExternControl Request"
        msg = comm.getExternControl()
        time.sleep(0.05)
        print "Sent"
        
        print "Sending external control commands..."
# atencion esto fue comentado alex
#        while True:
            #print comm.waitForLn()
        
 #           for i in range(200):
  #              comm.changeGas(0)
                
   #         for i in range(200):
    #            comm.changeGas(100)
                
     #       for i in range(200):
      #          comm.changeGas(200)
                
            #comm.changeNick(60)
            
            #comm.changeRoll(60)
            
            #comm.changeYaw(60)
            
            #comm.changeAll(nick=60,roll=60,yaw=60,gas=100,height=30)
        
    except Exception,e:
        print
        print "An error occured: ", e
        print
        traceback.print_exc()
        raw_input("Press ENTER, the application will close")
        
