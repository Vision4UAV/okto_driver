#! /usr/bin/env python

from MkMsg import *

# Must be connected to FC for this to produce desired response
class EngineTestMsg(MkMsg):
    def __init__(self, motorspeeds):
        self.address = MkMsg.FC_ADDRESS
        self.cmd = 't'
        # list of 12 values
        self.data = motorspeeds
