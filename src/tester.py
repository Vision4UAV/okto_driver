#!/usr/bin/env python

import roslib; roslib.load_manifest('okto_driver')
import rospy
import time
from okto_driver.msg import OktoCommand


def tester():
    pub = rospy.Publisher('okto_commands', OktoCommand)
    rospy.init_node('tester')
    while not rospy.is_shutdown():
        print "in loop"
        cmd = OktoCommand(0,0,0,0,0)
        for i in range(100):
            pub.publish(cmd)
            time.sleep(0.01)
            
        cmd.gas = 100
        for i in range(100):
            pub.publish(cmd)
            time.sleep(0.01)
            
        cmd.gas = 200
        for i in range(100):
            pub.publish(cmd)
            time.sleep(0.01)
            
            
if __name__ == '__main__':
    try:
        tester()
    except rospy.ROSInterruptException:
        pass
