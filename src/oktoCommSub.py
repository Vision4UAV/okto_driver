#!/usr/bin/env python

import roslib; roslib.load_manifest('okto_driver')
import rospy
import sys
from okto_driver.msg import OktoCommand
from MKComm import mkComm


comm = mkComm.MkComm()

def callback(data):
    comm.changeAll(nick=data.nick, roll=data.roll, yaw=data.dyaw, gas=data.gas, height=data.height)

def listener():
    rospy.init_node('okto')
    rospy.Subscriber("okto_commands", OktoCommand, callback)
    rospy.spin()


if __name__ == '__main__':
    comm.open("/dev/ttyUSB0")
    #if len(sys.argv) == 1:
    #    comm.open("/dev/ttyUSB0")
    #else:
    #    comm.open(sys.argv[0])
    comm.redirectNCToFC()
    comm.getExternControl()
    
    listener()
