#!/usr/bin/env python

import roslib; roslib.load_manifest('okto_driver')
import rospy

import sys
from okto_driver.msg import OktoSensorData
from MKComm import mkComm


comm = mkComm.MkComm()

def talker():
    pub = rospy.Publisher("okto_sensor_data", OktoSensorData)
    rospy.init_node('okto')
    while not rospy.is_shutdown():
        try:
            debugOut = comm.getDebugData()
            
            sensorData = OktoSensorData()
            sensorData.ang_nick = debugOut.getAngNick()
            sensorData.ang_roll = debugOut.getAngRoll()
            sensorData.acc_nick = debugOut.getAccNick()
            sensorData.acc_roll = debugOut.getAccRoll()
            sensorData.yaw_gyro = debugOut.getYawGyro()
            sensorData.altitude = debugOut.getAltitude()
            sensorData.acc_z = debugOut.getAccZ()
            sensorData.compass_heading = debugOut.getCompassHeading()
            sensorData.voltage = debugOut.getVoltage()
            sensorData.gyro_compass_heading = debugOut.getGyroCompassHeading()
            sensorData.current = debugOut.getCurrent()
            sensorData.capacity = debugOut.getCapacity()
            sensorData.gps_nick = debugOut.getGPSNick()
            sensorData.gps_roll = debugOut.getGPSRoll()
            
            pub.publish(sensorData)
            rospy.sleep(0.1)
        except Exception, e:
            traceback.print_exc()
        
        
if __name__ == '__main__':
    if len(sys.argv) == 1:
        comm.open("/dev/ttyUSB0")
    else:
        comm.open(sys.argv[0])
    comm.redirectNCToFC()
    comm.getExternControl()
    
    try:
        talker()
    except rospy.ROSInterruptException:
        pass
