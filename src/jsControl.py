#!/usr/bin/env python

import roslib; roslib.load_manifest('oktoDriver')
import rospy
import time
from threading import Lock
from sensor_msgs.msg import Joy
from oktoDriver.msg import OktoCommand


class JSControl(object):
    def __init__(self):
        rospy.init_node('joystick_convert')
        
        self.cmd = OktoCommand()
        self.lock = Lock()
        
        self.pub = rospy.Publisher("okto_commands", OktoCommand)
        rospy.Subscriber("joy", Joy, self.convert)
        
    def convert(self, msg):
        with self.lock:
            self.cmd = OktoCommand(int(msg.axes[1]*127), int(msg.axes[0]*127), int(msg.axes[2]*127), int((msg.axes[3]+1)*127.5), 0)
            
    def run(self):
        while not rospy.is_shutdown():
            with self.lock:
                self.pub.publish(self.cmd)
            time.sleep(0.05)
            
            
if __name__ == '__main__':
    jsControl = JSControl()
    jsControl.run()
