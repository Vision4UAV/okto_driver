#!/usr/bin/env python

import roslib; roslib.load_manifest('okto_driver')
import rospy
import sys
import traceback
from threading import Lock
from okto_driver.msg import OktoCommand
from okto_driver.msg import OktoSensorData
from MKComm import mkComm


class OktoComm(object):
    def __init__(self, commPort):
        self.commLock = Lock()
        self.commPort = commPort
        self.comm = mkComm.MkComm()
        
        rospy.init_node('okto')
        
        self.pub = rospy.Publisher("okto_sensor_data", OktoSensorData)
        rospy.Subscriber("okto_commands", OktoCommand, self.moveOkto)
        
    def moveOkto(self, msg):
        with self.commLock:
            self.comm.changeAll(nick=msg.nick, roll=msg.roll, yaw=msg.dyaw, gas=msg.gas, height=msg.height)
        
    def run(self):
        with self.commLock:
            self.comm.open(self.commPort)
            self.comm.redirectNCToFC()
            self.comm.getExternControl()
        
        while not rospy.is_shutdown():
            try:
                with self.commLock:
                    debugOut = self.comm.getDebugData()
            
                sensorData = OktoSensorData()
                sensorData.ang_nick = debugOut.getAngNick()
                sensorData.ang_roll = debugOut.getAngRoll()
                sensorData.acc_nick = debugOut.getAccNick()
                sensorData.acc_roll = debugOut.getAccRoll()
                sensorData.yaw_gyro = debugOut.getYawGyro()
                sensorData.altitude = debugOut.getAltitude()
                sensorData.acc_z = debugOut.getAccZ()
                sensorData.compass_heading = debugOut.getCompassHeading()
                sensorData.voltage = debugOut.getVoltage()
                sensorData.gyro_compass_heading = debugOut.getGyroCompassHeading()
                sensorData.current = debugOut.getCurrent()
                sensorData.capacity = debugOut.getCapacity()
                sensorData.gps_nick = debugOut.getGPSNick()
                sensorData.gps_roll = debugOut.getGPSRoll()
            
                self.pub.publish(sensorData)
                rospy.sleep(0.1)
            except Exception, e:
                traceback.print_exc()
            

if __name__ == '__main__':
    serial_usb_port = rospy.get_param("okto_driver/comm_port", "/dev/ttyUSB0")
    oktoComm = OktoComm(serial_usb_port)

    oktoComm.run()
